transformers==4.2.0
tensorboardX==1.8
tensorflow  # for tensorboardX
spacy
numpy
transformers
tqdm
torch==1.7.1+cu101
torchvision==0.8.2+cu101
torchaudio==0.7.2
jupyterlab
skimage
pandas
matplotlib